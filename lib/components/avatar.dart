import 'package:flutter/material.dart';
import 'package:spotify/core/const.dart';
import 'package:spotify/helper/extension.dart';

class Avatar extends StatelessWidget {
  final String? url;
  final double? size;
  final EdgeInsetsGeometry? margin;
  const Avatar({super.key, this.url, this.size, this.margin});

  @override
  Widget build(BuildContext context) {
    final defaultSize = context.deviceWidth * 0.18;
    return Container(
      margin: margin,
      width: defaultSize,
      height: defaultSize,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Const.primaryColor,
        image: DecorationImage(
          image: AssetImage(url ?? 'assets/images/user.png'),
        ),
      ),
    );
  }
}
