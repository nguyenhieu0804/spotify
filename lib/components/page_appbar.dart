import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:spotify/core/app_icon.dart';
import 'package:spotify/core/app_image.dart';

class PageAppBar extends StatelessWidget {
  final String leadingIconUrl;
  final String actionsIconUrl;
  final double leadingIconSize;
  final double actionsIconSize;
  final String? title;
  final bool showLeadingIcon;

  const PageAppBar({
    Key? key,
    this.title,
    this.leadingIconUrl = AppIcons.search,
    this.actionsIconUrl = AppIcons.setting,
    this.showLeadingIcon = true,
    this.leadingIconSize = 24,
    this.actionsIconSize = 24,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appBarHeight = AppBar().preferredSize.height;

    return SizedBox(
      height: appBarHeight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Visibility(
            visible: showLeadingIcon,
            child: SvgPicture.asset(
              leadingIconUrl,
              height: leadingIconSize.w,
              width: leadingIconSize.w,
              fit: BoxFit.cover,
            ),
          ),
          if (title != null)
            Text(
              title ?? '',
              style: Theme.of(context).textTheme.headline1,
            )
          else
            Image.asset(
              AppImages.appLogo,
              height: 40.h,
              width: 133.w,
            ),
          SvgPicture.asset(
            actionsIconUrl,
            fit: BoxFit.cover,
            height: actionsIconSize.w,
            width: actionsIconSize.w,
          ),
        ],
      ),
    );
  }
}
