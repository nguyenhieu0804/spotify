import 'package:flutter/material.dart';

class KeepAliveWidgt extends StatefulWidget {
  const KeepAliveWidgt({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;
  @override
  State<KeepAliveWidgt> createState() => _KeepAliveWidgtState();
}

class _KeepAliveWidgtState extends State<KeepAliveWidgt>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }

  @override
  bool get wantKeepAlive => true;
}
