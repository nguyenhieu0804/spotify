import 'package:flutter/material.dart';
import 'package:spotify/core/const.dart';

class LineWidget extends StatelessWidget {
  const LineWidget({
    Key? key,
    this.margin,
  }) : super(key: key);
  final EdgeInsetsGeometry? margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      height: 16,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [Const.backgroundColor, Const.greyColor.withOpacity(0.7)],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            tileMode: TileMode.clamp),
      ),
    );
  }
}
