import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/helper/extension.dart';

class SquareHorizontalItem extends StatelessWidget {
  final double? imageSize;
  final TextStyle? titleStyle;
  final TextStyle? subStyle;
  final String? imageUrl;
  final String? title;
  final String? subTitle;
  final bool showMoreIcon;
  final EdgeInsetsGeometry? margin;
  const SquareHorizontalItem(
      {super.key,
      this.imageSize,
      this.titleStyle,
      this.subStyle,
      this.imageUrl,
      this.title,
      this.subTitle,
      this.showMoreIcon = true,
      this.margin});

  @override
  Widget build(BuildContext context) {
    final size = context.deviceWidth * 0.18;
    return Container(
      margin: margin,
      height: imageSize ?? size,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(right: 15.w),
            width: imageSize ?? size,
            height: imageSize ?? size,
            decoration: BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.circular(12.w),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  imageUrl ?? 'assets/images/song_2.png',
                ),
              ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title ?? '',
                  style: titleStyle ??
                      Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.copyWith(height: 2),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  subTitle ?? '',
                  style: subStyle ??
                      Theme.of(context)
                          .textTheme
                          .bodyText2
                          ?.copyWith(height: 1.5),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.start,
                ),
              ],
            ),
          ),
          if (showMoreIcon)
            Icon(
              Icons.more_vert_rounded,
              color: Colors.white,
              size: 16.w,
            ),
        ],
      ),
    );
  }
}
