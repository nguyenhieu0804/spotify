import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:spotify/core/app_icon.dart';
import 'package:spotify/helper/extension.dart';

class SquareVertcalItem extends StatelessWidget {
  final double? imageSize;
  final TextStyle? titleStyle;
  final TextStyle? subStyle;
  final String? imageUrl;
  final String? title;
  final String? subTitle;
  final bool showIconPlay;
  const SquareVertcalItem({
    super.key,
    this.imageSize,
    this.titleStyle,
    this.subStyle,
    this.showIconPlay = true,
    this.imageUrl,
    this.title,
    this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    final size = context.deviceWidth * .3;

    return SizedBox(
      width: imageSize ?? size,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: const Alignment(0.9, 0.9),
            height: imageSize ?? size,
            width: imageSize ?? size,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.w),
              color: Colors.amber,
              image: DecorationImage(
                  image: AssetImage(imageUrl ?? ''), fit: BoxFit.cover),
            ),
            child: Visibility(
              visible: showIconPlay,
              child: SvgPicture.asset(
                AppIcons.play,
                height: 20.w,
                width: 20.w,
              ),
            ),
          ),
          Text(
            title ?? '',
            style: titleStyle ??
                Theme.of(context).textTheme.subtitle1?.copyWith(height: 2),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            subTitle ?? '',
            style: subStyle ??
                Theme.of(context).textTheme.subtitle2?.copyWith(height: 1.5),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.start,
          ),
        ],
      ),
    );
  }
}
