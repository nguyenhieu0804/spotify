import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/helper/extension.dart';
import 'package:spotify/model/artist_model.dart';

class RoundHorizontalItem extends StatelessWidget {
  final ArtistModel? artist;
  final double? imageSize;
  const RoundHorizontalItem({Key? key, this.artist, this.imageSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = context.deviceWidth * .2;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(right: 16.w),
          height: imageSize ?? size,
          width: imageSize ?? size,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.amber,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(artist?.avatar ?? ''),
            ),
          ),
        ),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                artist?.name ?? ' ',
                style:
                    Theme.of(context).textTheme.bodyText1?.copyWith(height: 2),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                '${artist?.monthlyListeners ?? 0} monthly listeners',
                style: Theme.of(context).textTheme.subtitle2,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
        ),
        Icon(
          Icons.arrow_forward_ios,
          color: Colors.white,
          size: 20.w,
        )
      ],
    );
  }
}
