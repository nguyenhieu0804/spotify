import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:spotify/core/app_icon.dart';

class SimpleButton extends StatelessWidget {
  final double? iconSize;
  final String? iconUrl;
  final String? title;
  final TextStyle? titleStyle;
  final VoidCallback? onPressed;
  const SimpleButton({
    Key? key,
    this.iconUrl,
    this.title,
    this.iconSize,
    this.onPressed,
    this.titleStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            iconUrl ?? AppIcons.profileAdd,
            height: iconSize ?? 40.w,
            width: iconSize ?? 40.w,
          ),
          Text(
            title ?? 'Find friend',
            style: titleStyle ??
                Theme.of(context).textTheme.bodyText2?.copyWith(height: 2),
          ),
        ],
      ),
    );
  }
}
