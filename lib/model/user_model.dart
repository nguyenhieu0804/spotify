import 'dart:convert';

class UserModel {
  final String? id;
  final String? name;
  final String? mail;
  final String? avatar;
  final int? followers;
  final int? following;
  UserModel({
    this.id,
    this.name,
    this.mail,
    this.avatar,
    this.followers,
    this.following,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'] != null ? json['id'] as String : null,
      name: json['name'] != null ? json['name'] as String : null,
      mail: json['mail'] != null ? json['mail'] as String : null,
      avatar: json['avatar'] != null ? json['avatar'] as String : null,
      followers: json['followers'] != null ? json['followers'] as int : null,
      following: json['following'] != null ? json['following'] as int : null,
    );
  }

  UserModel copyWith({
    String? id,
    String? name,
    String? mail,
    String? avatar,
    int? followers,
    int? following,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      mail: mail ?? this.mail,
      avatar: avatar ?? this.avatar,
      followers: followers ?? this.followers,
      following: following ?? this.following,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'mail': mail,
      'avatar': avatar,
      'followers': followers,
      'following': following,
    };
  }

  String toJson() => json.encode(toMap());
}
