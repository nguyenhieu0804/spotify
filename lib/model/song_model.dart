import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'artist_model.dart';

class SongModel {
  final String? id;
  final String? name;
  final ArtistModel? artist;
  final String? thumb;

  SongModel({
    this.id,
    this.name,
    this.artist,
    this.thumb,
  });

  factory SongModel.fromJson(json) {
    return SongModel(
      id: json['id'] != null ? json['id'] as String : null,
      name: json['name'] != null ? json['name'] as String : null,
      artist: json['artist'] != null
          ? ArtistModel.fromJson(json['artist'] as Map<String, dynamic>)
          : null,
      thumb: json['thumb'] != null ? json['thumb'] as String : null,
    );
  }
}

class SongModelList {
  final List<SongModel>? list;

  SongModelList({this.list});

  factory SongModelList.fromJson(List datas) {
    List<SongModel> list = [];

    try {
      list = datas.map((f) => SongModel.fromJson(f)).toList();
    } catch (e, stack) {
      debugPrint('$e - $stack');
    }
    return SongModelList(list: list);
  }
}
