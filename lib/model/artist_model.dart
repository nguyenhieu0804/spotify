import 'package:flutter/material.dart';

class ArtistModel {
  final String? id;
  final String? name;
  final String? avatar;
  final int? monthlyListeners;
  ArtistModel({
    this.id,
    this.name,
    this.avatar,
    this.monthlyListeners,
  });

  factory ArtistModel.fromJson(Map<String, dynamic> json) {
    return ArtistModel(
      id: json['id'] != null ? json['id'] as String : null,
      name: json['name'] != null ? json['name'] as String : null,
      avatar: json['avatar'] != null ? json['avatar'] as String : null,
      monthlyListeners: json['monthlyListeners'] != null
          ? json['monthlyListeners'] as int
          : null,
    );
  }
}

class ArtistModelList {
  final List<ArtistModel>? list;

  ArtistModelList({this.list});

  factory ArtistModelList.fromJson(List datas) {
    List<ArtistModel> list = [];

    try {
      list = datas.map((f) => ArtistModel.fromJson(f)).toList();
    } catch (e, stack) {
      debugPrint('$e - $stack');
    }
    return ArtistModelList(list: list);
  }
}
