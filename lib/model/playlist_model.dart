import 'package:flutter/material.dart';
import 'package:spotify/model/song_model.dart';

class PlaylistModel {
  final String? id;
  final String? name;
  final String? thumb;
  final List<SongModel>? songs;
  PlaylistModel({
    this.id,
    this.name,
    this.thumb,
    this.songs,
  });

  factory PlaylistModel.fromJson(Map<String, dynamic> json) {
    return PlaylistModel(
      id: json['id'] != null ? json['id'] as String : null,
      name: json['name'] != null ? json['name'] as String : null,
      thumb: json['thumb'] != null ? json['thumb'] as String : null,
      songs: json['songs'] != null
          ? SongModelList.fromJson(json['songs']).list
          : null,
    );
  }
}

class PlaylistModelList {
  final List<PlaylistModel>? list;

  PlaylistModelList({this.list});

  factory PlaylistModelList.fromJson(List datas) {
    List<PlaylistModel> list = [];

    try {
      list = datas.map((playlist) => PlaylistModel.fromJson(playlist)).toList();
    } catch (e, stack) {
      debugPrint('$e - $stack');
    }
    return PlaylistModelList(list: list);
  }
}
