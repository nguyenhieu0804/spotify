import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/components/item/square_horizontal_item.dart';
import 'package:spotify/helper/extension.dart';

import 'package:spotify/model/playlist_model.dart';

class BuildHistoryItem extends StatelessWidget {
  const BuildHistoryItem({Key? key, required this.playlist}) : super(key: key);

  final PlaylistModel? playlist;
  @override
  Widget build(BuildContext context) {
    final size = context.deviceWidth * 0.15;
    int length = 0;
    final songs = playlist?.songs;
    if (songs != null && songs.isNotEmpty) length = songs.length;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        SquareHorizontalItem(
          margin: EdgeInsets.only(top: 16.h),
          title: playlist?.name ?? '',
          imageUrl: playlist?.thumb ?? "assets/images/song_1.png",
          subTitle: 'playlist',
          showMoreIcon: false,
        ),
        ...List.generate(
          length > 2 ? 2 : length,
          (index) {
            final song = songs![index];
            return SquareHorizontalItem(
              margin: EdgeInsets.only(left: 12.w, top: 5),
              imageSize: size,
              title: song.name,
              imageUrl: song.thumb ?? '',
              subTitle: song.artist?.name ?? '',
              showMoreIcon: true,
            );
          },
        ),
        if (length > 2)
          Container(
            margin: EdgeInsets.only(left: 12.w, top: 20.h),
            child: Row(
              children: [
                Text(
                  'See all ${songs?.length} played',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                const Spacer(),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: 15.w,
                )
              ],
            ),
          )
      ],
    );
  }
}
