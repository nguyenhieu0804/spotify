import 'package:flutter/material.dart';
import 'package:spotify/components/page_appbar.dart';
import 'package:spotify/core/app_icon.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/data/history_local_data_sources.dart';
import 'package:spotify/model/playlist_model.dart';
import 'package:spotify/page/history/components/build_history_item.dart';
import '../../components/line_widget.dart';

class HistoryPage extends StatelessWidget {
  const HistoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Padding(
            padding: AppStyle.symHorizontal32,
            child: const PageAppBar(
              actionsIconUrl: AppIcons.more,
              showLeadingIcon: false,
              title: 'History',
            ),
          ),
          const LineWidget(),
          ListView(
            shrinkWrap: true,
            padding: AppStyle.symHorizontal32,
            children: [
              Text(
                'Today',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    ?.copyWith(fontWeight: FontWeight.normal),
              ),
              FutureBuilder(
                initialData: PlaylistModel(),
                future: HistoryLocalDataSources.instance.getHistoryToday(),
                builder: (context, snapshot) {
                  final playlist = snapshot.data;

                  return BuildHistoryItem(playlist: playlist);
                },
              ),
              AppStyle.sizedBoxH30,
              Text(
                'Yesterday',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    ?.copyWith(fontWeight: FontWeight.normal),
              ),
              FutureBuilder(
                initialData: PlaylistModel(),
                future: HistoryLocalDataSources.instance.getHistoryYesterday(),
                builder: (context, snapshot) {
                  final playlist = snapshot.data;

                  return BuildHistoryItem(playlist: playlist);
                },
              ),
              AppStyle.sizedBoxH30,
            ],
          ),
        ],
      ),
    );
  }
}
