import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/components/item/button/simple_button.dart';
import 'package:spotify/components/line_widget.dart';
import 'package:spotify/components/page_appbar.dart';
import 'package:spotify/components/sliver_app_bar_delegate.dart';
import 'package:spotify/core/app_icon.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/helper/extension.dart';
import 'package:spotify/page/profile/components/my_info.dart';
import 'components/build_mostly_played.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    final appBarHeight = AppBar().preferredSize.height;

    final myInfoHeight =
        (context.deviceHeight * 0.45) - appBarHeight + context.statusBarHeight;
    final controller = ScrollController();
    bool visible = false;

    return Column(
      children: [
        StatefulBuilder(
          builder: (context, setState) {
            controller.addListener(() {
              if (controller.offset >= myInfoHeight) {
                visible = true;
              } else {
                visible = false;
              }
              setState(() {});
            });
            return Visibility(
              visible: visible,
              child: Container(
                height: appBarHeight + context.statusBarHeight,
                padding: EdgeInsets.only(
                    left: 32.w, right: 32.w, top: context.statusBarHeight),
                child: const PageAppBar(
                  title: 'Profile',
                  actionsIconUrl: AppIcons.more,
                  showLeadingIcon: false,
                ),
              ),
            );
          },
        ),
        Expanded(
          child: NestedScrollView(
            controller: controller,
            headerSliverBuilder: (_, innerBoxIsScrolled) {
              return [
                SliverToBoxAdapter(child: MyInfo(height: myInfoHeight)),
                SliverToBoxAdapter(child: AppStyle.sizedBoxH24),
                SliverToBoxAdapter(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      SimpleButton(),
                      SimpleButton(iconUrl: AppIcons.share),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: LineWidget(
                    margin: EdgeInsets.only(top: 24.h),
                  ),
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: SliverAppBarDelegate(
                    28.sp + 40.h,
                    Padding(
                      padding: EdgeInsets.fromLTRB(32.w, 24.h, 32.w, 0),
                      child: Text(
                        "Mostly played",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: const BuildMostlyPlayed(),
          ),
        ),
      ],
    );
  }
}
