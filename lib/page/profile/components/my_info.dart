import 'package:flutter/material.dart';

import 'package:spotify/components/avatar.dart';
import 'package:spotify/components/page_appbar.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/core/const.dart';
import 'package:spotify/data/profile_local_data_sources.dart';
import 'package:spotify/helper/extension.dart';
import 'package:spotify/model/user_model.dart';

import '../../../core/app_icon.dart';

class MyInfo extends StatelessWidget {
  const MyInfo({Key? key, required this.height}) : super(key: key);

  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: context.statusBarHeight),
      height: height,
      decoration: BoxDecoration(
        color: Const.greyColor,
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(height * 0.15),
        ),
      ),
      child: FutureBuilder(
        initialData: UserModel(),
        future: ProfileLocalDataSources.instance.getMyProfile(),
        builder: (context, snapshot) {
          final user = snapshot.data;

          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: AppStyle.symHorizontal32,
                child: const PageAppBar(
                  title: 'Profile',
                  actionsIconUrl: AppIcons.more,
                  showLeadingIcon: false,
                ),
              ),
              AppStyle.sizedBoxH24,
              Avatar(
                url: user?.avatar,
              ),
              AppStyle.sizedBoxH16,
              Text(
                user?.name ?? '',
                style: Theme.of(context).textTheme.headline1,
              ),
              Text(
                user?.mail ?? '',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    ?.copyWith(height: 1.5),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _FollowItem(title: 'follwers', total: user?.followers ?? 0),
                  _FollowItem(title: 'following', total: user?.following ?? 0),
                ],
              ),
              SizedBox(height: height * 0.1)
            ],
          );
        },
      ),
    );
  }
}

class _FollowItem extends StatelessWidget {
  final String title;
  final int total;

  const _FollowItem({Key? key, required this.title, required this.total})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.subtitle2,
        ),
        Text(
          total.toString(),
          style: Theme.of(context).textTheme.headline1?.copyWith(height: 1.5),
        ),
      ],
    );
  }
}
