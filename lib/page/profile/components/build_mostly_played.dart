import 'package:flutter/material.dart';
import 'package:spotify/components/item/square_horizontal_item.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/data/profile_local_data_sources.dart';
import 'package:spotify/helper/extension.dart';
import 'package:spotify/model/song_model.dart';

class BuildMostlyPlayed extends StatelessWidget {
  const BuildMostlyPlayed({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      initialData: const <SongModel>[],
      future: ProfileLocalDataSources.instance.getMostlyPlayed(),
      builder: (context, snapshot) {
        final songs = snapshot.data;
        return ListView.builder(
          shrinkWrap: true,
          padding: AppStyle.symHorizontal32,
          itemCount: songs?.length,
          itemBuilder: (context, index) {
            final song = songs![index];
            return SquareHorizontalItem(
              margin: const EdgeInsets.only(bottom: 5),
              imageSize: context.deviceWidth * 0.15,
              imageUrl: song.thumb,
              title: song.name,
              subTitle: song.artist?.name ?? '',
            );
          },
        );
      },
    );
  }
}
