import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/components/item/round_horizontal_item.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/data/home_local_data_sources.dart';
import 'package:spotify/model/artist_model.dart';

class BuildHomeArtits extends StatelessWidget {
  const BuildHomeArtits({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: AppStyle.symHorizontal32,
      child: FutureBuilder(
        initialData: const <ArtistModel>[],
        future: HomeLocalDataSources.instance.getHomeArtists(),
        builder: (context, snapshot) {
          return Column(
            children: snapshot.data!
                .map(
                  (artist) => Padding(
                    padding: EdgeInsets.only(bottom: 12.h),
                    child: RoundHorizontalItem(
                      artist: artist,
                    ),
                  ),
                )
                .toList(),
          );
        },
      ),
    );
  }
}
