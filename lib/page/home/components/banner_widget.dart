import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/core/app_image.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/core/const.dart';

class BannerWidget extends StatelessWidget {
  const BannerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: AppStyle.symHorizontal32,
      height: 152.h,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            padding: AppStyle.symHorizontal16,
            height: 128.h,
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12.w),
                color: Const.bannerColor),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const _Icon(icon: Icons.arrow_back_ios),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Popular',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Padding(
                      padding: AppStyle.symVertical6,
                      child: Text(
                        'Sisa Rasa',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Text(
                      'Mahalini',
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
                const Spacer(),
                const _Icon(icon: Icons.arrow_forward_ios),
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 26.w,
            child: SizedBox(
              height: 152.h,
              width: 144.w,
              child: Image.asset(AppImages.bannerBgImg),
            ),
          )
        ],
      ),
    );
  }
}

class _Icon extends StatelessWidget {
  const _Icon({Key? key, this.icon}) : super(key: key);
  final IconData? icon;
  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      size: 20,
      color: Colors.white.withOpacity(.4),
    );
  }
}
