import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/components/item/square_vertcal_item.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/data/home_local_data_sources.dart';
import 'package:spotify/model/song_model.dart';

class BuildTodayHits extends StatelessWidget {
  const BuildTodayHits({super.key});

  @override
  Widget build(BuildContext context) {
    List<SongModel>? songs;

    return SingleChildScrollView(
      padding: AppStyle.symHorizontal32,
      scrollDirection: Axis.horizontal,
      child: FutureBuilder(
        initialData: const <SongModel>[],
        future: HomeLocalDataSources.instance.getTodayHits(),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            songs = snapshot.data;
          }
          return Row(
            children: songs!
                .map(
                  (song) => Padding(
                    padding: EdgeInsets.only(right: 16.w),
                    child: SquareVertcalItem(
                      title: song.name ?? '',
                      subTitle: song.artist?.name ?? '',
                      imageUrl: song.thumb ?? '',
                    ),
                  ),
                )
                .toList(),
          );
        },
      ),
    );
  }
}
