import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:spotify/core/app_icon.dart';

class TabbarWidget extends StatelessWidget {
  const TabbarWidget({super.key, required this.changeTab});
  final Function(int) changeTab;

  @override
  Widget build(BuildContext context) {
    final listTitle = <String>['Artists', 'Album', 'Podcast', 'Genre'];
    final tabbarStream = StreamController<String>.broadcast();
    return SizedBox(
      height: 40,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(listTitle.length, (index) {
            return Padding(
              padding: EdgeInsets.only(right: 50.w),
              child: StreamBuilder<String>(
                initialData: listTitle[0],
                stream: tabbarStream.stream,
                builder: (context, snapshot) {
                  bool isSelect = snapshot.data == listTitle[index];
                  final textColor = Colors.white.withOpacity(isSelect ? 1 : .6);

                  return GestureDetector(
                    onTap: () {
                      tabbarStream.sink.add(listTitle[index]);
                      changeTab(index);
                    },
                    child: Column(
                      children: [
                        Text(
                          listTitle[index],
                          style:
                              Theme.of(context).textTheme.headline2?.copyWith(
                                    color: textColor,
                                  ),
                        ),
                        Visibility(
                          visible: isSelect,
                          child: SvgPicture.asset(AppIcons.ellipse),
                        ),
                      ],
                    ),
                  );
                },
              ),
            );
          }),
        ),
      ),
    );
  }
}
