import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:spotify/components/line_widget.dart';
import 'package:spotify/components/page_appbar.dart';
import 'package:spotify/components/sliver_app_bar_delegate.dart';
import 'package:spotify/core/app_style.dart';
import 'components/banner_widget.dart';
import 'components/build_home_artits.dart';
import 'components/build_today_hits.dart';
import 'components/tabbar_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final pageController = PageController(initialPage: 0);
    return SafeArea(
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverPersistentHeader(
              pinned: true,
              delegate: SliverAppBarDelegate(
                AppBar().preferredSize.height,
                Container(
                  margin: EdgeInsets.only(right: 32.w),
                  padding: AppStyle.symHorizontal32,
                  child: const PageAppBar(),
                ),
              ),
            ),
            const SliverToBoxAdapter(child: BannerWidget()),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(
                    top: 32.h, bottom: 12.h, left: 32.w, right: 32.w),
                child: Text(
                  "Today's hits",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
            ),
            const SliverToBoxAdapter(child: BuildTodayHits()),
            SliverToBoxAdapter(child: AppStyle.sizedBoxH30),
            SliverPersistentHeader(
              pinned: true,
              delegate: SliverAppBarDelegate(
                66,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 32.w),
                      child: TabbarWidget(
                        changeTab: (intdex) =>
                            pageController.jumpToPage(intdex),
                      ),
                    ),
                    const LineWidget()
                  ],
                ),
              ),
            ),
          ];
        },
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: pageController,
          children: const [
            BuildHomeArtits(),
            BuildHomeArtits(),
            BuildHomeArtits(),
            BuildHomeArtits(),
          ],
        ),
      ),
    );
  }
}
