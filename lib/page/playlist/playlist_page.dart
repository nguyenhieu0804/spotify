import 'package:flutter/material.dart';

import 'package:spotify/components/item/square_vertcal_item.dart';
import 'package:spotify/components/line_widget.dart';
import 'package:spotify/components/page_appbar.dart';
import 'package:spotify/core/app_icon.dart';
import 'package:spotify/core/app_style.dart';
import 'package:spotify/data/playlist_local_data_sources.dart';
import 'package:spotify/helper/extension.dart';
import 'package:spotify/model/playlist_model.dart';

class PlaylistPage extends StatelessWidget {
  const PlaylistPage({super.key});

  @override
  Widget build(BuildContext context) {
    final sizeWidth = context.deviceWidth * 0.5;
    final imageSize = sizeWidth - 55;
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: AppStyle.symHorizontal32,
            child: SizedBox(
              height: AppBar().preferredSize.height,
              child: const PageAppBar(
                title: 'Playlist',
                actionsIconUrl: AppIcons.add,
                actionsIconSize: 32,
              ),
            ),
          ),
          const LineWidget(
            margin: EdgeInsets.only(bottom: 16),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: AppStyle.symHorizontal32,
              child: FutureBuilder(
                initialData: const <PlaylistModel>[],
                future: PlaylistLocalDataSources.instance.getFeaturedPlaylist(),
                builder: (context, snapshot) {
                  return Wrap(
                    runSpacing: 20,
                    spacing: 35,
                    children: snapshot.data!
                        .map(
                          (playlist) => SquareVertcalItem(
                            imageSize: imageSize,
                            imageUrl: playlist.thumb,
                            showIconPlay: false,
                            title: playlist.name,
                            subTitle: '${playlist.songs?.length} songs',
                          ),
                        )
                        .toList(),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
