import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:spotify/core/app_icon.dart';
import 'package:spotify/page/history/history_page.dart';
import 'package:spotify/page/home/home_page.dart';
import 'package:spotify/page/playlist/playlist_page.dart';
import 'package:spotify/page/profile/profile_page.dart';

import 'components/keep_alive_widget.dart';
import 'core/app_theme.dart';
import 'core/const.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(428, 926),
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: AppTheme.lightTheme,
          home: const MainView(),
        );
      },
    );
  }
}

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  final _pageController = PageController(initialPage: 0);
  final _bottomItemController = StreamController<int>.broadcast();

  late final List<Widget> _botomItems = [
    _bottomitem(0, AppIcons.homeOn, AppIcons.homeOff, 'Home'),
    _bottomitem(1, AppIcons.playlistOn, AppIcons.playlistOff, 'Playlist'),
    _bottomitem(2, AppIcons.historyOn, AppIcons.historyOff, 'History'),
    _bottomitem(3, AppIcons.profileOn, AppIcons.profileOff, 'Profile'),
  ];

  final List<Widget> _pages = const [
    KeepAliveWidgt(child: HomePage()),
    KeepAliveWidgt(child: PlaylistPage()),
    KeepAliveWidgt(child: HistoryPage()),
    KeepAliveWidgt(child: ProfilePage()),
  ];

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        elevation: 10,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        onPressed: () {},
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Image.asset(
            AppIcons.app,
            fit: BoxFit.cover,
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Const.greyColor,
        shape: const CircularNotchedRectangle(),
        child: SizedBox(
          height: 80.h,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _botomItems,
          ),
        ),
      ),
      body: PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: _pages,
      ),
    );
  }

  Widget _bottomitem(int index, String iconOn, String iconOff, String title) {
    return GestureDetector(
      onTap: () => _jumpPage(index),
      child: StreamBuilder(
        initialData: 0,
        stream: _bottomItemController.stream,
        builder: (context, snapshot) {
          bool isSelect = index == snapshot.data;
          return Column(
            children: [
              SvgPicture.asset(
                AppIcons.ellipse,
                height: 5.h,
                width: 5.h,
                color: isSelect ? null : Colors.transparent,
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.h),
                child: SvgPicture.asset(
                  isSelect ? iconOn : iconOff,
                  height: 30.h,
                  width: 30.h,
                ),
              ),
              Text(
                title,
                style: Theme.of(context).textTheme.subtitle2?.copyWith(
                      color: isSelect
                          ? Const.primaryColor
                          : Colors.white.withOpacity(.6),
                      height: 2,
                    ),
              ),
            ],
          );
        },
      ),
    );
  }

  _jumpPage(int index) {
    _pageController.jumpToPage(index);
    _bottomItemController.sink.add(index);
  }
}
