import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import '../model/playlist_model.dart';

class PlaylistLocalDataSources {
  static final _instance = PlaylistLocalDataSources();
  static PlaylistLocalDataSources get instance => _instance;

  Future<List<PlaylistModel>?> getFeaturedPlaylist() async {
    try {
      List<PlaylistModel>? playlists;

      final response = await rootBundle
          .loadString('assets/jsons/playlist/featured_playlist.json');
      final data = await json.decode(response);

      if (data['data'] != null) {
        playlists = PlaylistModelList.fromJson(data['data']).list;
      }

      return playlists;
    } catch (e) {
      debugPrint(' getFeaturedPlaylist error :$e');
      return Future.error(e);
    }
  }
}
