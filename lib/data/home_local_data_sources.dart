import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import '../model/artist_model.dart';
import '../model/song_model.dart';

class HomeLocalDataSources {
  static final _instance = HomeLocalDataSources();
  static HomeLocalDataSources get instance => _instance;

  Future<List<ArtistModel>?> getHomeArtists() async {
    try {
      List<ArtistModel>? artists;

      final response =
          await rootBundle.loadString('assets/jsons/home/artist.json');
      final data = await json.decode(response);

      if (data['data'] != null) {
        artists = ArtistModelList.fromJson(data['data']).list;
      }

      return artists;
    } catch (e) {
      debugPrint(' getHomeArtists error :$e');
      return Future.error(e);
    }
  }

  Future<List<SongModel>?> getTodayHits() async {
    try {
      List<SongModel>? songs;

      final response =
          await rootBundle.loadString('assets/jsons/home/today_hits.json');

      final data = await json.decode(response);

      if (data['data'] != null) {
        songs = SongModelList.fromJson(data['data']).list;
      }

      return songs;
    } catch (e) {
      debugPrint(' getTodayHits error :$e');
      return Future.error(e);
    }
  }
}
