import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../model/playlist_model.dart';

class HistoryLocalDataSources {
  static final _instance = HistoryLocalDataSources();
  static HistoryLocalDataSources get instance => _instance;

  Future<PlaylistModel?> getHistoryToday() async {
    try {
      PlaylistModel? playlists;

      final response = await rootBundle
          .loadString('assets/jsons/history/history_today.json');
      final data = await json.decode(response);

      if (data['data'] != null) {
        playlists = PlaylistModel.fromJson(data['data']);
      }

      return playlists;
    } catch (e) {
      debugPrint(' getHistoryToday error :$e');
      return Future.error(e);
    }
  }

  Future<PlaylistModel?> getHistoryYesterday() async {
    try {
      PlaylistModel? playlists;

      final response = await rootBundle
          .loadString('assets/jsons/history/history_yesterday.json');
      final data = await json.decode(response);

      if (data['data'] != null) {
        playlists = PlaylistModel.fromJson(data['data']);
      }

      return playlists;
    } catch (e) {
      debugPrint(' getHistoryYesterday error :$e');

      return Future.error(e);
    }
  }
}
