import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import '../model/song_model.dart';
import '../model/user_model.dart';

class ProfileLocalDataSources {
  static final _instance = ProfileLocalDataSources();
  static ProfileLocalDataSources get instance => _instance;

  Future<List<SongModel>?> getMostlyPlayed() async {
    try {
      List<SongModel>? songs;

      final response = await rootBundle
          .loadString('assets/jsons/profile/mostly_played.json');

      final data = await json.decode(response);

      if (data['data'] != null) {
        songs = SongModelList.fromJson(data['data']).list;
      }

      return songs;
    } catch (e) {
      debugPrint(' getTodayHits error :$e');
      return Future.error(e);
    }
  }

  Future<UserModel?> getMyProfile() async {
    try {
      UserModel? user;

      final response =
          await rootBundle.loadString('assets/jsons/profile/my_profile.json');

      final data = await json.decode(response);

      if (data['data'] != null) {
        user = UserModel.fromJson(data['data']);
      }

      return user;
    } catch (e) {
      debugPrint(' getMyProfile error :$e');
      return Future.error(e);
    }
  }
}
