import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyle {
  // EdgeInsets
  static EdgeInsets get symHorizontal32 =>
      EdgeInsets.symmetric(horizontal: 32.w);

  static EdgeInsets get symHorizontal16 =>
      EdgeInsets.symmetric(horizontal: 16.w);

  static EdgeInsets get symVertical24 => EdgeInsets.symmetric(vertical: 24.h);
  static EdgeInsets get symVertical6 => EdgeInsets.symmetric(vertical: 6.h);

  // SizedBox
  static SizedBox get sizedBoxH8 => SizedBox(height: 8.h);
  static SizedBox get sizedBoxH16 => SizedBox(height: 16.h);
  static SizedBox get sizedBoxH24 => SizedBox(height: 24.h);
  static SizedBox get sizedBoxH30 => SizedBox(height: 30.h);
  static SizedBox get sizedBoxH48 => SizedBox(height: 48.h);
}
