import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import 'const.dart';

class AppTheme {
  static final ThemeData lightTheme = ThemeData(
    primaryColor: Const.primaryColor,
    scaffoldBackgroundColor: Const.backgroundColor,
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      backgroundColor: Const.greyColor,
    ),
    textTheme: TextTheme(
      headline1: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 24.sp,
        fontWeight: FontWeight.w700,
      ),
      headline2: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 20.sp,
        fontWeight: FontWeight.w600,
      ),
      bodyText1: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 18.sp,
        fontWeight: FontWeight.w600,
      ),
      bodyText2: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 16.sp,
        fontWeight: FontWeight.normal,
      ),
      subtitle1: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
      ),
      subtitle2: GoogleFonts.montserrat(
        color: Colors.white,
        fontSize: 12.sp,
        fontWeight: FontWeight.normal,
      ),
    ),
  );
}
