class AppIcons {
  static const String search = 'assets/icons/search.svg';
  static const String setting = 'assets/icons/setting.svg';
  static const String play = 'assets/icons/play.svg';
  static const String ellipse = 'assets/icons/ellipse.svg';
  static const String app = 'assets/icons/app.png';
  static const String homeOn = 'assets/icons/home_on.svg';
  static const String homeOff = 'assets/icons/home_off.svg';
  static const String playlistOff = 'assets/icons/playlist_off.svg';
  static const String playlistOn = 'assets/icons/playlist_on.svg';
  static const String historyOn = 'assets/icons/history_on.svg';
  static const String historyOff = 'assets/icons/history_off.svg';
  static const String profileOff = 'assets/icons/profile_off.svg';
  static const String profileOn = 'assets/icons/profile_on.svg';
  static const String add = 'assets/icons/add.svg';
  static const String more = 'assets/icons/more.svg';
  static const String profileAdd = 'assets/icons/profile_add.svg';
  static const String share = 'assets/icons/share.svg';
}
