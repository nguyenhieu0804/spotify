import 'package:flutter/material.dart';

class Const {
  static const Color primaryColor = Color(0xFF42C83C);
  static const Color backgroundColor = Color(0xFF000000);
  static const Color greyColor = Color(0xFF333333);
  static const Color bannerColor = Color(0xFFD7BD1E);
}
